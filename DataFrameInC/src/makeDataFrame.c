#include <Rdefines.h>

SEXP
R_makeDataFrame(SEXP r_n)
{

    SEXP ans;
    int n = INTEGER(r_n)[0], j;
    SEXP d, i, s;
    PROTECT(ans = NEW_LIST(3));
    SET_VECTOR_ELT(ans, 0, d = NEW_NUMERIC(n));
    SET_VECTOR_ELT(ans, 1, i = NEW_INTEGER(n));
    SET_VECTOR_ELT(ans, 2, s = NEW_CHARACTER(n));
    for(j = 0; j < n; j++) {
	REAL(d)[j] = j + .5;
	INTEGER(i)[j] = j;
	char buf[10];
	sprintf(buf, "xyz_%d", j+1);
	SET_STRING_ELT(s, j, mkChar(buf));
    }
    SET_CLASS(ans, ScalarString(mkChar("data.frame")));
    
    SEXP names;
    PROTECT(names = NEW_CHARACTER(3));
    for(j = 0; j < 3; j++) {
	char buf1[20];
	sprintf(buf1, "Variable%d", j + 1);
	SET_STRING_ELT(names, j, mkChar(buf1));
    }
    SET_NAMES(ans, names);

    PROTECT(names = NEW_INTEGER(n));
    for(j = 0; j < n; j++) {
	INTEGER(names)[j] = j+1;
    }
    SET_ATTR(ans, Rf_install("row.names"), names);

    UNPROTECT(3);
    return(ans);
}
