a=sample(1:100000,10000)
b=sample(1:100000,20000)

a=sample(1:100000,100)
b=sample(1:100000,200)

dyn.load("in.so")
GenerateCommon=
      function(a,b)
    {
            n_a = length(a)
                n_b = length(b)
                common=.C("array", integer(n_a),    ## It seems that use an empty vector here doesn't work
                                    as.integer(a),as.integer(n_a),
                                    as.integer(b),as.integer(n_b))[[1]]
                common[!common == 0]
        }

system.time(replicate(100000,a[a%in%b]))  ## in my case, 0.016s
system.time(replicate(1000,GenerateCommon(a,b))) ## in my case, 0.029s
