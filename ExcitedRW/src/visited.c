#include <stdio.h> // for printf


/* 
 This is somewhat more complicated by the fact that we keep all of the steps,
 including the current location, in a matrix. So we fetch the current
 position from the "end" of the matrix.
 However, to avoid re-allocating the matrix at each iteration, we pre-allocate
 it and then keep track of which row is the current "last" row corresponding
 to our current step.
 
 A matrix is stored as a long vector with the elements of the first column first,
 then the elements from the second column, and so on. 
 Accordingly, the i-th element in the second row is 
   steps [ i + numRows ]


 We store the answer (0 or 1) in the first element of the visited integer vector
 passed to this routine via the .C()


  steps  - the 2 column integer matrix
  nSteps - the number of steps taken so far
  nRows  - the number of rows in the matrix steps
  visited - a logical vector into which we place the result 1 or 0 
 */
void
haveVisited(int *steps, int *nSteps, int *nRows, int *visited)
{
    int numSteps = nSteps[0], numRows = nRows[0];
    int cur_x = steps[numSteps - 1],
        cur_y = steps[numRows + numSteps - 1],
        i;


    *visited = 0;

    for(i = 0; i < numSteps -1 ; i++) {
//	printf("%d/%d:  (%d, %d)  (%d, %d)\n", i, numSteps, steps[i], steps[i + numRows], cur_x, cur_y);
	if(steps[i] == cur_x && steps[i + numRows] == cur_y) {
	    *visited = 1;
//	    printf("got it\n");
	    return;
	}
    }
}
