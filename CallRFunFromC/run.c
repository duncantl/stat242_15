#include <Rdefines.h>

SEXP makeCall(SEXP fun);

/* 
 Here we pass the R expression that will call the R function .
 We just have to set the argument each time and then call Rf_eval()
 with this expression.
 */
SEXP
run(SEXP r_call, SEXP rn)
{
    int i, n;

    SEXP ans;
    int nprotects = 1;
    n = INTEGER(rn)[0];
    PROTECT(ans = NEW_LIST(n));

    if(TYPEOF(r_call) == CLOSXP) {
         PROTECT(r_call = makeCall(r_call));
	 nprotects++;
    }

    for(i = 0; i < n; i++) {
	SEXP tmp;
	SETCAR(CDR(r_call), ScalarInteger(i+1));
	tmp = Rf_eval(r_call, R_GlobalEnv);
	SET_VECTOR_ELT(ans, i, tmp);
    }
    
    UNPROTECT(nprotects);
    return(ans);
}



SEXP
makeCall(SEXP fun)
{
    SEXP call;
    PROTECT(call = Rf_allocVector(LANGSXP, 2));

    SETCAR(call, fun);
    SETCAR(CDR(call), ScalarInteger(0));

    /* TO  put a name on the argument, say i, we use
       SETTAG(CRD(call), Rf_install("i"));
    */
    UNPROTECT(1);
    return(call);
}
