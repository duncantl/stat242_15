/*
  .C("mywhich", as.logical(x), integer(length(x)), length(x))
 */

void
mywhich(const int *const x, int *ans, int *numEls)
{
    int i, ctr = 0, n = numEls[0];
    
    for(i = 0; i < n; i++)
	if(x[i]) {
	    ans[ctr++] = i+1;
	}
    
    numEls[0] = ctr;
}


#include <Rdefines.h>

SEXP
mywhich1(SEXP x)
{
    int i, ctr = 0, n;
    n = Rf_length(x);
    int *els = INTEGER(x);
    
    for(i = 0; i < n; i++)
	if(els[i]) ctr++;

    SEXP ans = NEW_INTEGER(ctr); // Rf_allocVector(INTSXP, ctr);
    int *ans_ptr = INTEGER(ans);
    PROTECT(ans);
    ctr = 0;
    for(i = 0; i < n; i++)
	if(els[i]) {
	    ans_ptr[ctr] = i + 1;
            ctr++;
	}
    UNPROTECT(1);
    return(ans);
}
